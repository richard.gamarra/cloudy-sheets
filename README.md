# Cloudy Sheets

## General

This application is meant to use a cloud DB to serve simple sheets for the user.

## Usage

### Prerequisites

Have access to a MongoDB Atlas (or similar, don't necessarily have to be cloud)
Have NodeJS installed

-   Clone the project locally

#### Backend

-   Go to `server` directory
-   run `npm i`
-   copy `.env.sample` to a new file called `.env` and fill it with your DB URL
-   run `npm start`

### Frontend

-   Go to the `client` directory
-   run `npm i`
-   copy `.env.sample` to a new file called `.env`
-   run `npm start`

### Notes

-   In the server `.env` file feel free to change the por number for the API
    but, remember to change it in the client `.env` file too.
-   The client will ask for a port if port `3000` is in use.

### Captures

-   Here's a
    [video link](https://www.loom.com/share/8e288797dfd54d8db66fe8765e4110a1).
