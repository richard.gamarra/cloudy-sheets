import { Request, Response } from 'express'
import Sheet from '../models/sheet'

const create = (req: Request, res: Response) => {
    const { name } = req.body

    const sheet = new Sheet({
        name
    })

    return sheet
        .save()
        .then((sheet) => res.status(201).json({ sheet }))
        .catch((error) => res.status(500).json({ error }))
}

const read = (req: Request, res: Response) => {
    const id = req.params.id

    return Sheet.findById(id)
        .then((sheet) => (sheet ? res.status(200).json({ sheet }) : res.status(404).json({ message: 'sheet not found' })))
        .catch((error) => res.status(500).json({ error }))
}

const readAll = (req: Request, res: Response) => {
    return Sheet.find()
        .then((sheets) => res.status(200).json({ sheets }))
        .catch((error) => res.status(500).json({ error }))
}

const update = (req: Request, res: Response) => {
    const id = req.params.id

    return Sheet.findById(id)
        .then((sheet) => {
            if (sheet) {
                sheet.set(req.body)

                return sheet
                    .save()
                    .then((sheet) => res.status(201).json({ sheet }))
                    .catch((error) => res.status(500).json({ error }))
            } else {
                return res.status(404).json({ message: 'sheet not found' })
            }
        })
        .catch((error) => res.status(500).json({ error }))
}

const remove = (req: Request, res: Response) => {
    const id = req.params.id

    return Sheet.findById(id, function (error: any, doc: typeof Sheet) {
        if (error) res.status(500).json({ error })

        doc.remove()
            .then((sheet) => (sheet ? res.status(201).json({ sheet, message: 'sheet deleted' }) : res.status(404).json({ message: 'sheet not found' })))
            .catch((error) => res.status(500).json({ error }))
    })
}

export default { create, read, readAll, update, remove }
