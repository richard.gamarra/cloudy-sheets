import { Request, Response } from 'express'
import logger from '../library/logger'
import Column, { validTypes } from '../models/column'

const create = (req: Request, res: Response) => {
    const { sheet, headerTitle, columnType } = req.body
    const typeOptions = Object.values(validTypes)

    if (!typeOptions.includes(columnType))
        return res
            .status(401)
            .json({ error: `invalid columnType. Options are: ${typeOptions}` })

    const column = new Column({
        sheet,
        headerTitle,
        columnType
    })

    return column
        .save()
        .then((column) => res.status(201).json({ column }))
        .catch((error) => res.status(500).json({ error }))
}

const read = (req: Request, res: Response) => {
    const id = req.params.id

    return Column.findById(id)
        .populate('sheet')
        .then((column) =>
            column
                ? res.status(200).json({ column })
                : res.status(404).json({ message: 'column not found' })
        )
        .catch((error) => res.status(500).json({ error }))
}

const readAll = (req: Request, res: Response) => {
    const sheetId = req.params.id
    if (!sheetId)
        res.status(500).json({
            error: { message: 'You must provide a sheetId' }
        })

    return Column.find({
        sheet: sheetId
    })
        .then((columns) => res.status(200).json({ columns }))
        .catch((error) => res.status(500).json({ error }))
}

const update = (req: Request, res: Response) => {
    const id = req.params.id

    return Column.findById(id)
        .then((column) => {
            if (column) {
                column.set(req.body)

                return column
                    .save()
                    .then((column) => res.status(201).json({ column }))
                    .catch((error) => res.status(500).json({ error }))
            } else {
                return res.status(404).json({ message: 'column not found' })
            }
        })
        .catch((error) => res.status(500).json({ error }))
}

const remove = (req: Request, res: Response) => {
    const id = req.params.id

    return Column.findById(id, function (error: any, doc: typeof Column) {
        if (error) res.status(500).json({ error })

        doc.remove()
            .then((column) =>
                column
                    ? res
                          .status(201)
                          .json({ column, message: 'column deleted' })
                    : res.status(404).json({ message: 'column not found' })
            )
            .catch((error) => res.status(500).json({ error }))
    })
}

export default { create, read, readAll, update, remove }
