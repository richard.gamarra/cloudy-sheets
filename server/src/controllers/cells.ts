import { Request, Response } from 'express'
import Cell from '../models/cell'

const create = (req: Request, res: Response) => {
    const { column, value } = req.body

    const cell = new Cell({
        column,
        value
    })

    return cell
        .save()
        .then((cell) => res.status(201).json({ cell }))
        .catch((error) => res.status(500).json({ error }))
}

const read = (req: Request, res: Response) => {
    const id = req.params.id

    return Cell.findById(id)
        .populate('column')
        .then((cell) => (cell ? res.status(200).json({ cell }) : res.status(404).json({ message: 'cell not found' })))
        .catch((error) => res.status(500).json({ error }))
}

const readAll = (req: Request, res: Response) => {
    const columnId = req.params.id
    if (!columnId) res.status(500).json({ error: { message: 'You must provide a columnId' } })

    return Cell.find({
        column: columnId
    })
        .then((cells) => res.status(200).json({ cells }))
        .catch((error) => res.status(500).json({ error }))
}

const update = (req: Request, res: Response) => {
    const id = req.params.id

    return Cell.findById(id)
        .then((cell) => {
            if (cell) {
                cell.set(req.body)

                return cell
                    .save()
                    .then((cell) => res.status(201).json({ cell }))
                    .catch((error) => res.status(500).json({ error }))
            } else {
                return res.status(404).json({ message: 'cell not found' })
            }
        })
        .catch((error) => res.status(500).json({ error }))
}

const remove = (req: Request, res: Response) => {
    const id = req.params.id

    return Cell.findByIdAndDelete(id)
        .then((cell) => (cell ? res.status(201).json({ cell, message: 'cell removed' }) : res.status(404).json({ message: 'cell not found' })))
        .catch((error) => res.status(500).json({ error }))
}

export default { create, read, readAll, update, remove }
