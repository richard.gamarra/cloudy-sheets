import express from 'express'
import http from 'http'
import mongoose from 'mongoose'
import { config } from './config/config'
import logger from './library/logger'
import routes from './routes'

const router = express()

/** Connect to Mongo */
mongoose
    .connect(config.mongo.url, { retryWrites: true, w: 'majority' })
    .then(() => {
        logger.info('Mongo connected successfully.')
        startServer()
    })
    .catch((error) => logger.error(error))

/** Only Start Server if Mongoose Connects */
const startServer = () => {
    router.use((req, res, next) => {
        logger.info(`Incomming - METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`)

        res.on('finish', () => {
            logger.info(`Result - METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}] - STATUS: [${res.statusCode}]`)
        })

        next()
    })

    router.use(express.urlencoded({ extended: true }))
    router.use(express.json())

    /** Rules of our API */
    router.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')

        if (req.method == 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
            return res.status(200).json({})
        }

        next()
    })

    /** Routes */
    router.use('/api/sheets', routes.sheetRoutes)
    router.use('/api', routes.cellRoutes)
    router.use('/api', routes.columnRoutes)

    /** Healthcheck */
    router.get('/', (_req, res) => res.status(200).json({ message: 'connected' }))

    /** Error handling */
    router.use((_req, res) => {
        const error = new Error('Not found')

        logger.error(error)

        res.status(404).json({
            message: error.message
        })
    })

    http.createServer(router).listen(config.server.port, () => logger.info(`Server is running on port ${config.server.port}`))
}
