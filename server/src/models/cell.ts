import mongoose, { Document, Schema } from 'mongoose'

export interface ICell {
    column: string
    value: string
}

export interface ICellModel extends ICell, Document {}

const CellSchema: Schema = new Schema(
    {
        column: { type: Schema.Types.ObjectId, required: true, ref: 'Column' },
        value: { type: String, required: false }
    },
    {
        timestamps: true,
        versionKey: false
    }
)

export default mongoose.model<ICellModel>('Cell', CellSchema)
