import mongoose, { Document, Schema } from 'mongoose'

export interface ISheet {
    name: string
}

export interface ISheetModel extends ISheet, Document {}

const SheetSchema: Schema = new Schema(
    {
        name: { type: String, required: true }
    },
    {
        timestamps: true,
        versionKey: false
    }
)

SheetSchema.pre('remove', function (next) {
    this.model('Column').remove({ sheet: this._id }, next)
})

export default mongoose.model<ISheetModel>('Sheet', SheetSchema)
