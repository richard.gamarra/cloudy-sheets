import mongoose, { Document, Schema } from 'mongoose'
import Cell from '../models/cell'

export enum validTypes {
    'string',
    'number'
}

export interface IColumn {
    headerTitle: String
    columnType: validTypes
    sheet: String
}

export interface IColumnModel extends IColumn, Document {}

const ColumnSchema: Schema = new Schema(
    {
        headerTitle: { type: String, required: true },
        columnType: { type: String, required: true },
        sheet: { type: Schema.Types.ObjectId, required: true, ref: 'Sheet' }
    },
    {
        timestamps: true,
        versionKey: false
    }
)

ColumnSchema.post('save', function (doc) {
    const cells = Array(100).fill({
        column: doc._id,
        value: ''
    })

    Cell.insertMany(cells)
})

ColumnSchema.pre('remove', function (next) {
    this.model('Cell').remove({ column: this._id }, next)
})

export default mongoose.model<IColumnModel>('Column', ColumnSchema)
