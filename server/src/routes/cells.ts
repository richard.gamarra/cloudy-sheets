import express from 'express'
import controller from '../controllers/cells'

const router = express.Router()

router.post('/cells/', controller.create)
router.get('/cells/:id', controller.read)
router.get('/columns/:id/cells/', controller.readAll)
router.put('/cells/:id', controller.update)
router.delete('/cells/:id', controller.remove)

export = router
