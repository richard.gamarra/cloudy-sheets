import express from 'express'
import controller from '../controllers/sheets'

const router = express.Router()

router.post('/', controller.create)
router.get('/:id', controller.read)
router.get('/', controller.readAll)
router.put('/:id', controller.update)
router.delete('/:id', controller.remove)

export = router
