import sheetRoutes from './sheets'
import columnRoutes from './columns'
import cellRoutes from './cells'

export default { cellRoutes, columnRoutes, sheetRoutes }
