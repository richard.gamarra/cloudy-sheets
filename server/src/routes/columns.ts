import express from 'express'
import controller from '../controllers/columns'

const router = express.Router()

router.post('/columns/', controller.create)
router.get('/columns/:id', controller.read)
router.get('/sheets/:id/columns', controller.readAll)
router.put('/columns/:id', controller.update)
router.delete('/columns/:id', controller.remove)

export = router
