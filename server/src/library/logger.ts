import { createLogger, format, transports } from 'winston'
import util from 'util'

const { combine, timestamp, json, printf } = format
const inspect = (data: any) => util.inspect(data, { showHidden: true, depth: null, colors: true })
const customFormat = printf(({ level, message, timestamp }) => `\n${timestamp} [${level}]: ${inspect(message)}`)

const logger = createLogger({
    format: combine(
        format.colorize(),
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        json(),
        customFormat
    ),
    transports: [new transports.Console()]
})

export default logger
