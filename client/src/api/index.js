import axios from 'axios'

const server = axios.create({
    baseURL: process.env.REACT_APP_API_URL
})

/** Sheets **/
export const getSheetById = (id) => server.get(`/api/sheets/${id}`)
export const createSheet = (payload) => server.post(`/api/sheets`, payload)
export const updateSheetById = (id, payload) =>
    server.put(`/api/sheets/${id}`, payload)
export const deleteSheetById = (id) => server.delete(`/api/sheets/${id}`)
/** Columns **/
export const getSheetColumns = (id) => server.get(`/api/sheets/${id}/columns/`)
export const createColumn = (payload) => server.post(`/api/columns`, payload)
export const updateColumnById = (id, payload) =>
    server.put(`/api/columns/${id}`, payload)
export const deleteColumnById = (id) => server.delete(`/api/columns/${id}`)
/** Cells **/
export const getColumnCells = (id) => server.get(`/api/columns/${id}/cells`)
export const createCell = (payload) => server.post(`/api/cells`, payload)
export const updateCellById = (id, payload) =>
    server.put(`/api/cells/${id}`, payload)

const api = {
    createSheet,
    updateSheetById,
    deleteSheetById,
    getSheetById,
    getSheetColumns,
    createColumn,
    updateColumnById,
    deleteColumnById,
    getColumnCells,
    createCell,
    updateCellById
}

export default api
