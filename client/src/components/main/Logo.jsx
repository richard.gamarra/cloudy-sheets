import React from 'react'
import logo from '../../logo.svg'

const Wrapper = ({ children }) => {
    return <span className="navbar-brand">{children}</span>
}

const Logo = () => {
    return (
        <Wrapper>
            <img src={logo} width="50" height="50" alt="cloudy sheets logo" />
        </Wrapper>
    )
}

export default Logo
