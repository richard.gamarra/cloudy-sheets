import { Container, Row } from 'react-bootstrap'
import { Outlet } from 'react-router-dom'
import NavBar from './NavBar'

const Main = () => {
    return (
        <Container fluid>
            <Row>
                <NavBar />
            </Row>

            <Row>
                <Outlet />
            </Row>
        </Container>
    )
}

export default Main
