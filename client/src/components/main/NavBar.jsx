import React from 'react'
import { Navbar } from 'react-bootstrap'
import Logo from './Logo'

const Nav = ({ children }) => {
    return <Navbar className="navbar navbar-expand-lg text-white bg-dark">{children}</Navbar>
}

const NavBar = () => {
    return (
        <Nav>
            <Logo />
            <h1>Cloudy Sheets App</h1>
        </Nav>
    )
}

export default NavBar
