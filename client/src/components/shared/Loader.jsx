import { Spinner } from 'react-bootstrap'

const Loader = () => {
    return (
        <div className="d-flex justify-content-center">
            <Spinner
                className="float-right"
                variant="dark"
                animation="grow"
                size="xl"
            />
        </div>
    )
}

export default Loader
