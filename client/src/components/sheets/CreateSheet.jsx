import { useEffect } from 'react'
import { useState } from 'react'
import { Alert, Container } from 'react-bootstrap'
import moment from 'moment'
import api from '../../api'
import { Navigate } from 'react-router-dom'
import Loader from '../shared/Loader'

const Creating = () => {
    return (
        <>
            <h1 className="text-center">We're creating a new sheet for you</h1>
            <p className="text-center">please wait</p>
            <Loader />
        </>
    )
}

const TakeToSheet = ({ sheetId }) => {
    const [redirecting, setRedirecting] = useState(true)

    useEffect(() => {
        if (sheetId) {
            setTimeout(() => {
                setRedirecting(false)
            }, 2000)
        }
    }, [sheetId])

    return redirecting ? (
        <>
            <h1>{`Redirecting to your new sheet... ID: ${sheetId}`}</h1>
            <Loader />
        </>
    ) : (
        <Navigate to={`/sheets/${sheetId}`} />
    )
}

const CreateSheet = () => {
    const [loading, setLoading] = useState(true)
    const [sheetId, setSheetId] = useState(null)
    const [error, setError] = useState(null)
    const sheetName = moment().format('YYYY-mm-DD_HH-mm-ss')

    useEffect(() => {
        setTimeout(() => {
            api.createSheet({ name: `${sheetName}` })
                .then((response) => {
                    const { _id } = response.data.sheet
                    setSheetId(_id)
                })
                .catch((error) => setError(error.message))
                .finally(() => setLoading(false))
        }, 2000)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Container className="my-5">
            {error ? <Alert variant="danger">{error}</Alert> : null}
            {loading ? <Creating /> : <TakeToSheet sheetId={sheetId} />}
        </Container>
    )
}

export default CreateSheet
