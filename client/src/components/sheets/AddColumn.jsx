import { useState } from 'react'
import { Button, Col, Form, Modal } from 'react-bootstrap'
import api from '../../api'

const AddColumn = ({ sheetId, onSuccess }) => {
    const [showModal, setShowModal] = useState(false)
    const [title, setTitle] = useState('')
    const [type, setType] = useState('string')
    const handleCreateColumn = () => {
        api.createColumn({
            sheet: sheetId,
            headerTitle: title,
            columnType: type
        })
        setShowModal(false)
        onSuccess()
    }

    return (
        <>
            <Modal
                show={showModal}
                animation={true}
                contentClassName={'p-3'}
                onHide={() => {}}
            >
                <Modal.Header>
                    <h2>New Column</h2>
                </Modal.Header>
                <Modal.Body className="d-grid gap-4 mt-4">
                    <Form.Group>
                        <Form.Control
                            style={{ fontWeight: 'bold' }}
                            className="text-center"
                            type="text"
                            placeholder="Header title"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Select
                            style={{ fontWeight: 'bold' }}
                            className="text-center"
                            type="text"
                            placeholder="Column type"
                            value={type}
                            onChange={(e) => setType(e.target.value)}
                        >
                            <option value="string">string</option>
                            <option value="number">number</option>
                        </Form.Select>
                    </Form.Group>
                    <Button onClick={handleCreateColumn}>Add Column</Button>
                </Modal.Body>
            </Modal>
            <Col className="p-0">
                <Button onClick={() => setShowModal(true)}>+</Button>
            </Col>
        </>
    )
}

export default AddColumn
