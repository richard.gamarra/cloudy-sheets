import { useEffect, useState } from 'react'
import { Alert, Container, Row } from 'react-bootstrap'
import { useParams } from 'react-router-dom'
import api from '../../api'
import Loader from '../shared/Loader'
import AddColumn from './AddColumn'
import Column from './Column'

const Sheet = () => {
    const [loading, setLoading] = useState(true)
    const [columns, setColumns] = useState([])
    const [error, setError] = useState(null)
    const params = useParams()
    const sheetId = params.id
    const [fetchColumns, setFetchColumns] = useState(1)

    const Columns = ({ columns, afterRemove }) => {
        return (
            <Row style={{ disply: 'flex', justifyContent: 'left' }}>
                {columns.map((column) => {
                    return (
                        <Column
                            key={column._id}
                            column={column}
                            afterRemove={afterRemove}
                        />
                    )
                })}
                <AddColumn
                    sheetId={sheetId}
                    onSuccess={() => setFetchColumns(fetchColumns + 1)}
                />
            </Row>
        )
    }

    useEffect(() => {
        api.getSheetColumns(sheetId)
            .then((response) => {
                const { columns } = response.data
                setColumns(columns)
            })
            .catch((error) => setError(error.message))
            .finally(() => setLoading(false))
    }, [fetchColumns, sheetId])

    return (
        <Container fluid>
            {error ? <Alert variant="danger">{error}</Alert> : null}
            {loading ? (
                <Loader />
            ) : (
                <Columns
                    columns={columns}
                    afterRemove={() => setFetchColumns(fetchColumns + 1)}
                />
            )}
        </Container>
    )
}

export default Sheet
