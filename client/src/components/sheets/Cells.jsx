import { useState } from 'react'
import { useEffect } from 'react'
import { Form, FormGroup } from 'react-bootstrap'
import api from '../../api'

const Cells = ({ columnId, type }) => {
    const [cells, setCells] = useState([])

    const Cell = ({ id, value }) => {
        const [cellValue, setCellValue] = useState(value)

        const handleSaveCellValue = () => {
            api.updateCellById(id, { value: cellValue })
        }

        return (
            <FormGroup>
                <Form.Control
                    type={type}
                    value={cellValue}
                    onChange={(e) => setCellValue(e.target.value)}
                    onBlur={handleSaveCellValue}
                />
            </FormGroup>
        )
    }

    useEffect(() => {
        api.getColumnCells(columnId).then((response) => {
            setCells(response.data.cells)
        })
    }, [columnId])

    return cells.map((cell) => (
        <Cell key={cell._id} id={cell._id} value={cell.value} />
    ))
}

export default Cells
