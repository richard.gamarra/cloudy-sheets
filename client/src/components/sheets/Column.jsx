import { useState } from 'react'
import { Col, Form, InputGroup } from 'react-bootstrap'
import { Trash } from 'react-bootstrap-icons'
import api from '../../api'
import Cells from './Cells'

const Column = ({ column, afterRemove }) => {
    const { columnType: type, headerTitle: name, _id: id } = column

    const Header = () => {
        const [title, setTitle] = useState(name)

        const handleSaveTitle = () => {
            api.updateColumnById(id, { headerTitle: title })
        }

        const handleRemoveColumn = () => {
            if (window.confirm('Are you sure to remove this column?')) {
                api.deleteColumnById(id).then(() => afterRemove())
            }
        }

        return (
            <InputGroup>
                <Form.Control
                    style={{ fontWeight: 'bold' }}
                    className="text-center bg-light"
                    type="text"
                    placeholder="Header title"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    onBlur={handleSaveTitle}
                />
                <InputGroup.Text type={type} onClick={handleRemoveColumn}>
                    <span style={{ minWidth: '1rem' }} role="button">
                        <Trash />
                    </span>
                </InputGroup.Text>
            </InputGroup>
        )
    }

    return (
        <Col className="p-0" style={{ maxWidth: '18rem', width: '5rem' }}>
            <Header />
            <Cells columnId={id} type={type} />
        </Col>
    )
}

export default Column
