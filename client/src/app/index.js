import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { Main } from '../components/main'
import Sheet from '../components/sheets/Sheet'
import CreateSheet from '../components/sheets/CreateSheet'

function App() {
    return (
        <Router>
            <Main />
            <Routes>
                <Route path="/" exact element={<CreateSheet />} />
                <Route path="/sheets/:id" exact element={<Sheet />} />
            </Routes>
        </Router>
    )
}

export default App
